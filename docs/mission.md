---
title: Unsere Mission
# heroText: Unsere Mission
# tagline: 
prev: /
next: /aktuelles/
---
# Unsere Mission
Der Kinder- und Jugendclub Zörbig befindet sich unweit des Rudolf-Breitscheid-Parks in einem Flachbau mit 10 Räumen, die vielseitig genutzt werden können. Unter anderem steht eine gut ausgestattete Küche zur Verfügung. Kleine Snacks können zubereitet oder auch Kaffee und Tee gekocht werden. Somit besteht die Möglichkeit, die eine oder andere Party vorzubereiten.

In einem anderen Raum befinden sich zahlreiche Computer, an denen man seine Computerkenntnisse festigen und erweitern kann. Desweiteren kann man im Internet surfen, chatten oder Bewerbungen schreiben.

Die Anzahl der Räume im Jugendclub ermöglicht die Gliederung in drei Altersbereiche.
  
I.      Altersbereich      7-12 Jahre  
II.     Altersbereich    13-15 Jahre  
III.    Altersbereich    16-27 Jahre  
  
Aufgrund dieser Einteilung können die Betreuer besser auf die Bedürfnisse und Probleme der Kids und Teenager eingehen. Ferienbetreuung, Gespräche über Alkohol- und Drogenmissbrauch, praktisches Bewerbungstraining und die Unterstützung bei der Ausbildungs- und Arbeitsplatzsuche sind nur einige Themen des Jugendclubs Zörbig.
