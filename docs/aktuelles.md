---
title: Aktuelles
#heroText: Aktuelles
#tagline: 
prev: /
next: /ausschreibung/
---
# Aktuelles

### 13.07.2023 [Update 16.07.23] Hightlights, Gewinner & Platzierungen: Das war die 23. Curbici Veterano
Die bildlichen Hightlights der diesjährigen Oldtimerrallye stehen in der Rubrik "Rückblick" bereit, ebenso die Wertungslisten mit den Erstplatzierungen in der jeweiligen Fahrzeugklasse.  
  
Wir wünschen viel Spaß bei der Bilderbetrachtung!  
  
HINWEIS:  
Die 24. Curbici Veterano vom 13.07.-14.07.2024 sollte schon jetzt vorgemerkt werden!