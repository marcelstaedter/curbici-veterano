---
home: true
title: Curbici Veterano
heroImage: /images/header.jpg
heroText: Curbici Veterano
tagline: Das war die 23. Curbici Veterano / Zörbig
actions:
  - text: Aktuelles
    link: /aktuelles/
    type: primary
features:
  - title: Stadt Zörbig
    details: Curbici Veterano - Markt 12 - D - 06780 Zörbig
  - title: Telefon
    details: 034956 / 60 - 103
  - title: Telefax
    details: 034956 / 60 - 111
---
-- Hightlights & Wertungslisten der diesjährigen Rallye online--
