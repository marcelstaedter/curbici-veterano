import { defineUserConfig, defaultTheme } from 'vuepress'

export default defineUserConfig({
    title: 'Curbici Veterano',
    description: 'Curbici Veterano',
    dest: 'public',
    base: '/curbici-veterano/',
    lang: 'de-DE',
    theme: defaultTheme({    
      repo: 'https://gitlab.com/marcelstaedter/curbici-veterano/docs',
      repoLabel: 'GitLab',
      editLinks: true,
      editLinkText: 'Bearbeiten',
      contributors: false,
      contributorsText: 'Bearbeitet von',
      lastUpdated: true,
      lastUpdatedText: 'Zuletzt geändert',
      navbar: [
        { text: 'Start', link: '/' },
        { text: 'Aktuelles', link: '/aktuelles' },
        { text: 'Ausschreibung', link: '/ausschreibung' },
        { text: 'Anmeldung', link: '/anmeldung' },
        { text: 'Programm', link: '/programm' },
        { text: 'Anfahrt', link: '/anfahrt' },
        { text: 'Presse', link: '/presse' },
        { text: 'Rückblick', link: '/rueckblick' },
        { text: 'Impressum', link: '/impressum' },
        { text: 'Datenschutz', link: '/datenschutz' },
      ],
      sidebar: false,
      logo: '/images/logo.svg',
      footer: [
        {
          text: 'Impressum',
          link: '/impressum/',
        },
      ],
    })
  })
