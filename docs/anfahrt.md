---
title: Anfahrt
#heroText: Aktuelles
#tagline: 
prev: /programm/
next: /presse/
---
# Anfahrt
So erreichen Sie uns in Zörbig:  
  
über die A9 (Abfahrt 12 Zörbig/Wolfen)  
über die A2 Kreuz Magdeburg  
weiter über die A14 Richtung Halle (Abfahrt Zörbig / Halle-Tornau)  
dann weiter in Richtung Zörbig  
Navigationsadresse: Am Schloß 12, 06780 Zörbig  
  
Am Veranstaltungsort werden Sie von unseren Einweisern empfangen und auf Ihre Stellfläche geführt.