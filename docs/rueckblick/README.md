---
title: Rückblicke
#heroText: Rückblicke
#tagline: 
prev: /presse/
---
# Rückblicke
1. Curbici Veterano
11. und 12. Juli 1998
2. Curbici Veterano
17. und 18. Juli 1999
3. Curbici Veterano
15. und 16. Juli 2000
4. Curbici Veterano
14. und 15. Juli 2001
5. Curbici Veterano
13. und 14. Juli 2002
6. Curbici Veterano
12. und 13. Juli 2003
7. Curbici Veterano
10. und 11. Juli 2004
8. Curbici Veterano
9. und 10. Juli 2005
9. Curbici Veterano
8. und 9. Juli 2006
10. Curbici Veterano
7. und 8. Juli 2007
11. Curbici Veterano
12. und 13. Juli 2008
12. Curbici Veterano
11. und 12. Juli 2009
13. Curbici Veterano
10. und 11. Juli 2010
14. Curbici Veterano
9. und 10. Juli 2011
15. Curbici Veterano
7. und 8. Juli 2012
13. Curbici Veterano
10. und 11. Juli 2013
17. Curbici Veterano
12. und 13. Juli 2014
18. Curbici Veterano
11. und 12. Juli 2015
19. Curbici Veterano
9. und 10. Juli 2016
20. Curbici Veterano
8. und 9. Juli 2017
21. Curbici Veterano
7. und 8. Juli 2018
22. Curbici Veterano
13. und 14. Juli 2019
23. Curbici Veterano
8. und 9. Juli 2023
