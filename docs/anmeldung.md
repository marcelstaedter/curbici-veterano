---
title: Anmeldung
#heroText: Aktuelles
#tagline: 
prev: /ausschreibung/
next: /programm/
---
# Anmeldung
Informationen zur 24. Curbici Veterano erhalten Sie im 1. Quartal 2024.  
Bitte beachten Sie die Hinweise unter Aktuell