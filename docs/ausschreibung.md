---
title: Ausschreibung
#heroText: Aktuelles
#tagline: 
prev: /aktuelles/
next: /anmeldung/
---
# Ausschreibung
Informationen zur 24. Curbici Veterano erhalten Sie im 1. Quartal 2024.  
Bitte beachten Sie die Hinweise unter Aktuell