---
title: Impressum
heroText: Impressum
tagline: 
---
# Impressum
Veranstalter:
Stadt Zörbig
Markt 12
D - 06780 Zörbig

### Ansprechpartner
Tatjana Anton

Telefon: [034956 / 60-103](tel:03495660103)  
Telefax: 034956 / 60-111

E-Mail: [tatjana.anton@stadt-zoerbig.de](mailto:tatjana.anton@stadt-zoerbig.de)

### Homepage
[www.stadt-zoerbig.de](https://www.stadt-zoerbig.de)  
[www.curbiciveterano.de](https://www.curbiciveterano.de)

### Bankverbindung
Stadt Zörbig
IBAN: DE34800537220032180460
BIC: NOLADE21BTF
Kreissparkasse Anhalt-Bitterfeld

### Homepagegestaltung
Webmaster: Sebastian Herbsleb
Administrator: Michael Dittrich
E-Mail: curbici-veterano@gmx.net

### Darstellung
Die Webseite ist optimiert für den Internet Explorer, bei einer Bildschirmauflösung von 1024 x 768 Pixel.

### Benutzerhinweise
Die Fotos und Texte dieser Internetseite werden von den jeweiligen Autoren zur Verfügung gestellt.
Eine kommerzielle Verwendung des hier veröffentlichen Materials ist hiermit ausdrücklich untersagt!

Für alle auf dieser Seite angebrachten Links gilt der Haftungsausschluss!

Sollten Sie ein technisches Problem haben oder einen Link der nicht richtig funktioniert bemerken, so wenden Sie sich bitte mit einer E-Mail an den Administrator.

![Impressum](https://curbiciveterano.de/images/sb5.jpg)

## Veranstaltungsteam "OG Curbici Veterano"
![Das Team von "OG Curbici Veterano](https://curbiciveterano.de/images/gruppenbild2023.jpg)

## Sponsoren / Dank
Seit vielen Jahren übernimmt das Autohaus "König & Partner" aus Zörbig das Hauptsponsoring unserer Oldtimerrallyes und unterstützt damit die Vorbereitung und Durchführung der "Curbici Veterano". Wir bedanken uns für die Treue und großzügige Unterstützung!

![Hauptsponsor "König & Partner"](https://curbiciveterano.de/images/logo_sp1.jpg)

Viele ehrenamtliche Helfer sind im Team der OG Curbici Veterano zu den Veranstaltungen im Einsatz. Ihr Organisationstalent und Ihrer Kreativität ist es zu verdanken, dass die Veranstaltungen weit über die Stadt Zörbig hinaus großes Interesse und Beteiligung findet.
Die finanzielle Grundlage der Veranstaltung bilden großzügige Sach- und Geldspenden der ortsansässigen Firmen und Privatpersonen, ohne deren Hilfe und Zuverlässigkeit die jährlichen Veranstaltungen nicht möglich wären.

![Sponsoren Bild: Sponsoren](https://curbiciveterano.de/images/logo_sp2_2.jpg)
![Sponsoren Bild: Sponsoren](https://curbiciveterano.de/images/logo_sp3.jpg)